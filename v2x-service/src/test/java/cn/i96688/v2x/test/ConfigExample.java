package cn.i96688.v2x.test;

import java.util.Properties;
import java.util.concurrent.Executor;

import com.alibaba.nacos.api.NacosFactory;
import com.alibaba.nacos.api.PropertyKeyConst;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.config.listener.Listener;
import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.NamingFactory;
import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.api.naming.listener.Event;
import com.alibaba.nacos.api.naming.listener.EventListener;
import com.alibaba.nacos.api.naming.listener.NamingEvent;

/**
 * Config service example
 * 
 * @author Nacos
 *
 */
public class ConfigExample {

	public static void main(String[] args) throws NacosException, InterruptedException {
		try {
			testCusmer();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	static void testProvider() throws Exception{
		String serverAddr = "localhost";
		String dataId = "v2x-service";
		String group = "mapping-cn.i96688.v2x.module.msg.service.MessageService";
		Properties properties = new Properties();
		properties.put(PropertyKeyConst.SERVER_ADDR, serverAddr);
		ConfigService configService = NacosFactory.createConfigService(properties);
		String content = configService.getConfig(dataId, group, 5000);
		System.out.println(content);
		configService.addListener(dataId, group, new Listener() {
			@Override
			public void receiveConfigInfo(String configInfo) {
				System.out.println("recieve:" + configInfo);
			}

			@Override
			public Executor getExecutor() {
				return null;
			}
		});
		
		boolean isPublishOk = configService.publishConfig(dataId, group, "content");
		System.out.println("isPublishOk="+isPublishOk);
		
		Thread.sleep(3000);
		content = configService.getConfig(dataId, group, 5000);
		System.out.println("content1="+content);

		boolean isRemoveOk = configService.removeConfig(dataId, group);
		System.out.println("isRemoveOk="+isRemoveOk);
		Thread.sleep(3000);

		content = configService.getConfig(dataId, group, 5000);
		System.out.println("content2="+content);
		Thread.sleep(300000);

	}
	static void testCusmer() throws Exception{
		Properties properties = new Properties();
        properties.setProperty("serverAddr", System.getProperty("serverAddr","127.0.0.1"));
        properties.setProperty("namespace", System.getProperty("namespace","public"));

        NamingService naming = NamingFactory.createNamingService(properties);

        naming.registerInstance("consumers:cn.i96688.v2x.api.DataService::", "11.11.11.11", 8888, "TEST1");

        naming.registerInstance("consumers:cn.i96688.v2x.api.DataService::", "2.2.2.2", 9999, "DEFAULT");

        System.out.println(naming.getAllInstances("consumers:cn.i96688.v2x.api.DataService::"));

        naming.deregisterInstance("consumers:cn.i96688.v2x.api.DataService::", "2.2.2.2", 9999, "DEFAULT");

        System.out.println(naming.getAllInstances("consumers:cn.i96688.v2x.api.DataService::"));

        naming.subscribe("consumers:cn.i96688.v2x.api.DataService::", new EventListener() {
            @Override
            public void onEvent(Event event) {
                System.out.println(((NamingEvent)event).getServiceName());
                System.out.println(((NamingEvent)event).getInstances());
            }
        });
	}
}
