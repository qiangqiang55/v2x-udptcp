package cn.i96688.v2x.api;

import cn.i96688.v2x.v2xframe.Response;

/**
 * 登录
 * @author xys
 *
 */
public interface AuthService {
	Response login(String id,String password);
}
