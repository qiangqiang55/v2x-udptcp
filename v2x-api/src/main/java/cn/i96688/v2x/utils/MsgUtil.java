package cn.i96688.v2x.utils;

import javax.xml.bind.DatatypeConverter;

public class MsgUtil {
	//消息分隔符
    public static final byte DELIMITER = 0x7e;
    
	public static String hexBinary2str(byte[] v) {
		return DatatypeConverter.printHexBinary(v);
	}
	public static byte[] str2hexBinary(String str) {
		return DatatypeConverter.parseHexBinary(str);
	}
	
}
