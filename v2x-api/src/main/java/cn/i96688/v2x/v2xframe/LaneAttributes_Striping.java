/*
 * Generated by ASN.1 Java Compiler (https://www.asnlab.org/)
 * From ASN.1 module "V2xFrame"
 */
package cn.i96688.v2x.v2xframe;

import java.io.*;
import javax.validation.constraints.*;
import org.asnlab.asndt.runtime.conv.*;
import org.asnlab.asndt.runtime.conv.annotation.*;
import org.asnlab.asndt.runtime.type.AsnType;
import org.asnlab.asndt.runtime.value.*;
import org.asnlab.asndt.runtime.value.BitString;

public class LaneAttributes_Striping extends BitString {

	public static final int stripeToConnectingLanesRevocableLane = 0;
	public static final int stripeDrawOnLeft = 1;
	public static final int stripeDrawOnRight = 2;
	public static final int stripeToConnectingLanesLeft = 3;
	public static final int stripeToConnectingLanesRight = 4;
	public static final int stripeToConnectingLanesAhead = 5;

	public LaneAttributes_Striping(byte[] bytes, byte unusedBits) {
		super(bytes, unusedBits);
	}

	public LaneAttributes_Striping(int size) {
		super(size);
	}

	public LaneAttributes_Striping() {
		super(0);
	}

	public boolean getStripeToConnectingLanesRevocableLane(){
		return getBit(stripeToConnectingLanesRevocableLane);
	}

	public void setStripeToConnectingLanesRevocableLane(){
		setBit(stripeToConnectingLanesRevocableLane, true);
	}

	public void clearStripeToConnectingLanesRevocableLane(){
		setBit(stripeToConnectingLanesRevocableLane, false);
	}

	public boolean getStripeDrawOnLeft(){
		return getBit(stripeDrawOnLeft);
	}

	public void setStripeDrawOnLeft(){
		setBit(stripeDrawOnLeft, true);
	}

	public void clearStripeDrawOnLeft(){
		setBit(stripeDrawOnLeft, false);
	}

	public boolean getStripeDrawOnRight(){
		return getBit(stripeDrawOnRight);
	}

	public void setStripeDrawOnRight(){
		setBit(stripeDrawOnRight, true);
	}

	public void clearStripeDrawOnRight(){
		setBit(stripeDrawOnRight, false);
	}

	public boolean getStripeToConnectingLanesLeft(){
		return getBit(stripeToConnectingLanesLeft);
	}

	public void setStripeToConnectingLanesLeft(){
		setBit(stripeToConnectingLanesLeft, true);
	}

	public void clearStripeToConnectingLanesLeft(){
		setBit(stripeToConnectingLanesLeft, false);
	}

	public boolean getStripeToConnectingLanesRight(){
		return getBit(stripeToConnectingLanesRight);
	}

	public void setStripeToConnectingLanesRight(){
		setBit(stripeToConnectingLanesRight, true);
	}

	public void clearStripeToConnectingLanesRight(){
		setBit(stripeToConnectingLanesRight, false);
	}

	public boolean getStripeToConnectingLanesAhead(){
		return getBit(stripeToConnectingLanesAhead);
	}

	public void setStripeToConnectingLanesAhead(){
		setBit(stripeToConnectingLanesAhead, true);
	}

	public void clearStripeToConnectingLanesAhead(){
		setBit(stripeToConnectingLanesAhead, false);
	}

	public boolean equals(Object obj) {
		if(!(obj instanceof LaneAttributes_Striping)){
			return false;
		}
		return TYPE.equals(this, obj, CONV);
	}

	public void per_encode(boolean align, OutputStream out) throws IOException {
		TYPE.encode(this, align? EncodingRules.ALIGNED_PACKED_ENCODING_RULES:EncodingRules.UNALIGNED_PACKED_ENCODING_RULES, CONV, out);
	}

	public static LaneAttributes_Striping per_decode(boolean align, InputStream in) throws IOException {
		return (LaneAttributes_Striping)TYPE.decode(in, align? EncodingRules.ALIGNED_PACKED_ENCODING_RULES:EncodingRules.UNALIGNED_PACKED_ENCODING_RULES, CONV);
	}


	public final static AsnType TYPE = V2xFrame.type(65638);

	public final static AsnConverter CONV;

	static {
		CONV = new ReflectionBitStringConverter(LaneAttributes_Striping.class);
	}


}
