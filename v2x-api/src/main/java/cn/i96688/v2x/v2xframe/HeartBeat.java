/*
 * Generated by ASN.1 Java Compiler (https://www.asnlab.org/)
 * From ASN.1 module "V2xFrame"
 */
package cn.i96688.v2x.v2xframe;

import java.io.*;
import javax.validation.constraints.*;
import org.asnlab.asndt.runtime.conv.*;
import org.asnlab.asndt.runtime.conv.annotation.*;
import org.asnlab.asndt.runtime.type.AsnType;
import org.asnlab.asndt.runtime.value.*;

public class HeartBeat {

	@NotNull
	@Size(min=8, max=8)
	@Component(0)
	public byte[] id;


	public boolean equals(Object obj) {
		if(!(obj instanceof HeartBeat)){
			return false;
		}
		return TYPE.equals(this, obj, CONV);
	}

	public void per_encode(boolean align, OutputStream out) throws IOException {
		TYPE.encode(this, align? EncodingRules.ALIGNED_PACKED_ENCODING_RULES:EncodingRules.UNALIGNED_PACKED_ENCODING_RULES, CONV, out);
	}

	public static HeartBeat per_decode(boolean align, InputStream in) throws IOException {
		return (HeartBeat)TYPE.decode(in, align? EncodingRules.ALIGNED_PACKED_ENCODING_RULES:EncodingRules.UNALIGNED_PACKED_ENCODING_RULES, CONV);
	}


	public final static AsnType TYPE = V2xFrame.type(65676);

	public final static CompositeConverter CONV;

	static {
		CONV = new AnnotationCompositeConverter(HeartBeat.class);
		AsnConverter idConverter = OctetStringConverter.INSTANCE;
		CONV.setComponentConverters(new AsnConverter[] { idConverter });
	}


}
