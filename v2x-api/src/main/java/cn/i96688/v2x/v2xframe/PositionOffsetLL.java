/*
 * Generated by ASN.1 Java Compiler (https://www.asnlab.org/)
 * From ASN.1 module "V2xFrame"
 */
package cn.i96688.v2x.v2xframe;

import java.io.*;
import javax.validation.constraints.*;
import org.asnlab.asndt.runtime.conv.*;
import org.asnlab.asndt.runtime.conv.annotation.*;
import org.asnlab.asndt.runtime.type.AsnType;
import org.asnlab.asndt.runtime.value.*;

public class PositionOffsetLL {

	public static final int position_LLlChosen = 0;
	public static final int position_LL2Chosen = 1;
	public static final int position_LL3Chosen = 2;
	public static final int position_LL4Chosen = 3;
	public static final int position_LL5Chosen = 4;
	public static final int position_LL6Chosen = 5;
	public static final int position_LatlonChosen = 6;

	public final int choiceID;

	@Alternative(0)
	public final Position_LL_24B position_LLl;

	@Alternative(1)
	public final Position_LL_28B position_LL2;

	@Alternative(2)
	public final Position_LL_32B position_LL3;

	@Alternative(3)
	public final Position_LL_36B position_LL4;

	@Alternative(4)
	public final Position_LL_44B position_LL5;

	@Alternative(5)
	public final Position_LL_48B position_LL6;

	@Alternative(6)
	public final Position_LLmD_64b position_Latlon;


	private PositionOffsetLL(int choiceID, Position_LL_24B position_LLl, Position_LL_28B position_LL2, Position_LL_32B position_LL3, Position_LL_36B position_LL4, Position_LL_44B position_LL5, Position_LL_48B position_LL6, Position_LLmD_64b position_Latlon) {
		this.choiceID = choiceID;
		this.position_LLl = position_LLl;
		this.position_LL2 = position_LL2;
		this.position_LL3 = position_LL3;
		this.position_LL4 = position_LL4;
		this.position_LL5 = position_LL5;
		this.position_LL6 = position_LL6;
		this.position_Latlon = position_Latlon;
	}

	public static PositionOffsetLL position_LLl(Position_LL_24B position_LLl) {
		return new PositionOffsetLL(position_LLlChosen, position_LLl, null, null, null, null, null, null);
	}

	public static PositionOffsetLL position_LL2(Position_LL_28B position_LL2) {
		return new PositionOffsetLL(position_LL2Chosen, null, position_LL2, null, null, null, null, null);
	}

	public static PositionOffsetLL position_LL3(Position_LL_32B position_LL3) {
		return new PositionOffsetLL(position_LL3Chosen, null, null, position_LL3, null, null, null, null);
	}

	public static PositionOffsetLL position_LL4(Position_LL_36B position_LL4) {
		return new PositionOffsetLL(position_LL4Chosen, null, null, null, position_LL4, null, null, null);
	}

	public static PositionOffsetLL position_LL5(Position_LL_44B position_LL5) {
		return new PositionOffsetLL(position_LL5Chosen, null, null, null, null, position_LL5, null, null);
	}

	public static PositionOffsetLL position_LL6(Position_LL_48B position_LL6) {
		return new PositionOffsetLL(position_LL6Chosen, null, null, null, null, null, position_LL6, null);
	}

	public static PositionOffsetLL position_Latlon(Position_LLmD_64b position_Latlon) {
		return new PositionOffsetLL(position_LatlonChosen, null, null, null, null, null, null, position_Latlon);
	}

	public boolean equals(Object obj) {
		if(!(obj instanceof PositionOffsetLL)){
			return false;
		}
		return TYPE.equals(this, obj, CONV);
	}

	public void per_encode(boolean align, OutputStream out) throws IOException {
		TYPE.encode(this, align? EncodingRules.ALIGNED_PACKED_ENCODING_RULES:EncodingRules.UNALIGNED_PACKED_ENCODING_RULES, CONV, out);
	}

	public static PositionOffsetLL per_decode(boolean align, InputStream in) throws IOException {
		return (PositionOffsetLL)TYPE.decode(in, align? EncodingRules.ALIGNED_PACKED_ENCODING_RULES:EncodingRules.UNALIGNED_PACKED_ENCODING_RULES, CONV);
	}


	public final static AsnType TYPE = V2xFrame.type(65599);

	public final static ChoiceConverter CONV;

	static {
		CONV = new AnnotationChoiceConverter(PositionOffsetLL.class);
		AsnConverter position_LLlConverter = Position_LL_24B.CONV;
		AsnConverter position_LL2Converter = Position_LL_28B.CONV;
		AsnConverter position_LL3Converter = Position_LL_32B.CONV;
		AsnConverter position_LL4Converter = Position_LL_36B.CONV;
		AsnConverter position_LL5Converter = Position_LL_44B.CONV;
		AsnConverter position_LL6Converter = Position_LL_48B.CONV;
		AsnConverter position_LatlonConverter = Position_LLmD_64b.CONV;
		CONV.setAlternativeConverters(new AsnConverter[] { position_LLlConverter, position_LL2Converter, position_LL3Converter, position_LL4Converter, position_LL5Converter, position_LL6Converter, position_LatlonConverter });
	}


}
