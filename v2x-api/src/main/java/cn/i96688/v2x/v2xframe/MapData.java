/*
 * Generated by ASN.1 Java Compiler (https://www.asnlab.org/)
 * From ASN.1 module "V2xFrame"
 */
package cn.i96688.v2x.v2xframe;

import java.io.*;
import java.math.*;
import java.util.*;
import javax.validation.constraints.*;
import org.asnlab.asndt.runtime.conv.*;
import org.asnlab.asndt.runtime.conv.annotation.*;
import org.asnlab.asndt.runtime.type.AsnType;
import org.asnlab.asndt.runtime.value.*;

public class MapData {

	@NotNull
	@Component(0)
	public Integer msgCnt;

	@Null
	@Component(1)
	public Integer timeStamp;	/* OPTIONAL */

	@NotNull
	@Component(2)
	public Vector<Node> nodes;


	public boolean equals(Object obj) {
		if(!(obj instanceof MapData)){
			return false;
		}
		return TYPE.equals(this, obj, CONV);
	}

	public void per_encode(boolean align, OutputStream out) throws IOException {
		TYPE.encode(this, align? EncodingRules.ALIGNED_PACKED_ENCODING_RULES:EncodingRules.UNALIGNED_PACKED_ENCODING_RULES, CONV, out);
	}

	public static MapData per_decode(boolean align, InputStream in) throws IOException {
		return (MapData)TYPE.decode(in, align? EncodingRules.ALIGNED_PACKED_ENCODING_RULES:EncodingRules.UNALIGNED_PACKED_ENCODING_RULES, CONV);
	}


	public final static AsnType TYPE = V2xFrame.type(65653);

	public final static CompositeConverter CONV;

	static {
		CONV = new AnnotationCompositeConverter(MapData.class);
		AsnConverter msgCntConverter = MsgCount.CONV;
		AsnConverter timeStampConverter = MinuteOfTheYear.CONV;
		AsnConverter nodesConverter = NodeList.CONV;
		CONV.setComponentConverters(new AsnConverter[] { msgCntConverter, timeStampConverter, nodesConverter });
	}


}
