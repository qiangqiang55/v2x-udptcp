package cn.i96688.test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Scanner;

import com.google.gson.Gson;

import cn.i96688.v2x.v2xframe.Response;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.DatagramPacket;
import io.netty.channel.socket.nio.NioDatagramChannel;
import io.netty.util.AttributeKey;
import io.netty.util.internal.SocketUtils;

public class TestUdpClient {
	public static final String TOKEN = "8E1A5A50";
	public static String AESKEY="28D5ACA2EBED4E49";
	public static int PORT=8402;
	public static void main(String[] args) {
		
		ClientPool pool=new ClientPool();
		try {
			System.out.println("0：退出\n非0：发送BSM消息进行测试\n输入选项后按回车键\n");
			@SuppressWarnings("resource")
			Scanner scan = new Scanner(System.in);
			while(scan.hasNextInt()) {
				int i=scan.nextInt();
				if(i==0) {
					break;
				}
				pool.sendMsg(TestData.getBSM());
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		pool.shutdown();
	}
	
	static class ClientPool{
		EventLoopGroup group = new NioEventLoopGroup();
		Bootstrap b = new Bootstrap();
		Channel ch =null;
		UdpClientChannel handler=new UdpClientChannel();
		ClientPool(){
			try {
	            b.group(group)
	             .channel(NioDatagramChannel.class)
	             .option(ChannelOption.SO_BROADCAST, true)
	             .handler(new ChannelInitializer<NioDatagramChannel>() {

					@Override
					protected void initChannel(NioDatagramChannel ch) throws Exception {
						ch.pipeline().addLast(handler.getClass().getSimpleName(),handler);
					}});
				ch =b.bind(0).sync().channel();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		void sendMsg(byte[] msg) throws Exception {
			AttributeKey<Result> result = AttributeKey.valueOf("Result");
			ch.attr(result).set(new Result());
			ch.writeAndFlush(new DatagramPacket(
                    Unpooled.copiedBuffer(msg),
                    SocketUtils.socketAddress("localhost", PORT))).sync();
		}
		void shutdown(){
			group.shutdownGracefully();
		}
	}
	static class UdpClientChannel extends SimpleChannelInboundHandler<DatagramPacket>{

		@Override
		protected void channelRead0(ChannelHandlerContext ctx, DatagramPacket msg) throws Exception {
			AttributeKey<Result> result = AttributeKey.valueOf("Result");
			Result ta=ctx.channel().attr(result).get();
			if(ta!=null) {
				byte[] bs=new byte[msg.content().readableBytes()];
				msg.content().readBytes(bs);
				ta.test(bs);
			}
		}
		
	}
	static class Result{
		public void test(byte[] bs) {
			try {
				Response resp = Response.per_decode(false,new ByteArrayInputStream(bs));
				System.out.println("终端收到返回消息："+new  Gson().toJson(resp));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
