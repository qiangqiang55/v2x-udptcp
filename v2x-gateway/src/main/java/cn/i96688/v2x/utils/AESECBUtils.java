package cn.i96688.v2x.utils;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class AESECBUtils {

    private static final String ALGORITHM = "AES";
    private static final String TRANSFORMATION = "AES/ECB/PKCS5Padding";
    private static final String ENCODE_UTF_8 = "UTF-8";

    /**
     * run test
     *
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
//        String s = "hello,您好";
//        String key = "3jd92j28ahl9.W2_";

        String s="3A36AB2C";
        String key = "84E1E3AE75AFF902";
        //System.out.println("s:" + s);
        //System.out.println("key:" + key);

        byte[] data = new byte[]{7, -67, -128, 0, 0, -60, 58, -111, 67, -72, -128, -128, -48, -40, -30, -36, -2, 66,
                -118, -62, -107, -100, 88, -52, 0, -128, 100, 0, 0, -96, 1, -25, -65, 0, 125, 23, -47, -128, -128, 0, 1,
                0, -64, 8, 49, -19, 16, -16, 16, 32};
        // 解密后 中文，需要处理下，不然会出现问题

//        String s1 = AESECBUtils.encrypt(s, key);
//        System.out.println("加密:" + s1);
        try {
            String content = AESECBUtils.decrypt("kziogujpqOp8iPE8aYx+UA==", "84E1E3AE75AFF902");
            System.out.println("解密:" + content);

            //AESECBUtils.decrypt(data, key);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 加密
     *
     * @param input 加密的字符串
     * @param key   解密的key
     * @return HexString
     */

    public static String encrypt(String input, String key) throws Exception {

        byte[] crypted = null;
        crypted = encrypt(input.getBytes(ENCODE_UTF_8), key);
        // SecretKeySpec skey = new SecretKeySpec(key.getBytes(), ALGORITHM);
        //
        // Cipher cipher = Cipher.getInstance(TRANSFORMATION);
        // cipher.init(Cipher.ENCRYPT_MODE, skey);
        // crypted = cipher.doFinal(input.getBytes(ENCODE_UTF_8));
        return Base64.encodeBase64String(crypted);

    }

    public static byte[] encrypt(byte[] input, String key) throws Exception {

        byte[] crypted = null;
        SecretKeySpec skey = new SecretKeySpec(key.getBytes(), ALGORITHM);
        Cipher cipher = Cipher.getInstance(TRANSFORMATION);
        // 查看数据块位数 默认为16（byte） * 8 =128 bit
        //System.out.println("数据块位数(byte)：" + cipher.getBlockSize());
        cipher.init(Cipher.ENCRYPT_MODE, skey);
        crypted = cipher.doFinal(input);
        return crypted;
    }

    /**
     * 解密
     *
     * @param input 解密的字符串
     * @param key   解密的key
     * @return String
     */
    public static String decrypt(String input, String key) throws Exception {

        byte[] output = null;

        output = decrypt(Base64.decodeBase64(input), key);
        // SecretKeySpec skey = new SecretKeySpec(key.getBytes(), ALGORITHM);
        // Cipher cipher = Cipher.getInstance(TRANSFORMATION);
        // cipher.init(Cipher.DECRYPT_MODE, skey);
        // output = cipher.doFinal(Base64.decodeBase64(input));
        return new String(output, ENCODE_UTF_8);
    }

    public static byte[] decrypt(byte[] input, String key) throws Exception {
        byte[] output = null;
        SecretKeySpec skey = new SecretKeySpec(key.getBytes(), ALGORITHM);
        Cipher cipher = Cipher.getInstance(TRANSFORMATION);
        cipher.init(Cipher.DECRYPT_MODE, skey);
        output = cipher.doFinal(input);
        return output;
    }
}