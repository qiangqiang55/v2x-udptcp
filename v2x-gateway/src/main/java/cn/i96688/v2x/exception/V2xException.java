package cn.i96688.v2x.exception;

public class V2xException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1632386746870143059L;
	public V2xException(String errMsg) {
		super(errMsg);
	}
	public V2xException(Throwable e) {
		super(e);
	}
	public V2xException(String message, Throwable cause) {
        super(message, cause);
    }
}
