package cn.i96688.v2x.modules.auth;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import cn.i96688.v2x.api.AuthService;
import cn.i96688.v2x.utils.MsgUtil;
import cn.i96688.v2x.v2xframe.Login;
import cn.i96688.v2x.v2xframe.Response;

/**
 * 一个简单的鉴权模拟
 * @author xuyuansheng
 *
 */
@Component
public class AuthManager {
	@Value("${v2x.auth.cache.level}")
    private int level;
	@Value("${v2x.auth.cache.apacity}")
	private int apacity;
	@Value("${v2x.auth.cache.maximum}")
	private int maximum;
	@Value("${v2x.auth.cache.duration}")
	private int duration;
	@Reference(check=false)
	private AuthService authService;
	
	private Cache<String, Integer> cache = null;
	
	@PostConstruct
	public void init() {
		cache = CacheBuilder.newBuilder()
				// 设置并发级别为8，并发级别是指可以同时写缓存的线程数
				.concurrencyLevel(level)
				// 设置缓存容器的初始容量为100000
				.initialCapacity(apacity)
				// 设置缓存最大容量为10000000，超过10000000之后就会按照LRU最近虽少使用算法来移除缓存项
				.maximumSize(maximum)
				// 是否需要统计缓存情况,该操作消耗一定的性能,生产环境应该去除
				//.recordStats()
				// 设置写缓存后60分钟过期
				.expireAfterWrite(duration, TimeUnit.MINUTES)
				// build方法中可以指定CacheLoader，在缓存不存在时通过CacheLoader的实现自动加载缓存
				.build();
	}
	
	
	public Response login(Login login) {
		String id=MsgUtil.hexBinary2str(login.id);
		Response resp = authService.login(id, new String(login.password));
		if(resp.isSuccess()) {
			put(id);
		}
		return resp;
	}
	/**
	 * 是否登录
	 * @param key
	 * @return
	 */
	public boolean checkAuth(String key) {
		return cache.getIfPresent(key)!=null;
	}
	public void put(String key) {
		cache.put(key, 1);
	}
	@PreDestroy
	public void shutdown() {
		if(cache !=null) {
			cache.invalidateAll();
		}
	}
}
