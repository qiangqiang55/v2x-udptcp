package cn.i96688.v2x;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class V2XApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(V2XApplication.class).web(WebApplicationType.NONE).run(args);
    }

}
