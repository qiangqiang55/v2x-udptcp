# 车联网TCP-UDP数据接收网关

#### Description
车联网V2X数据接入实时性太高，一个中等城市并发量能到每秒百万至千万数据之间，数据传输协议使用TCP、UDP，所以数据接入网关使用netty非常适合。本程序使用netty实现TCP、UDP服务，数据报文协议采用ASN.1，运行框架采用spring cloud+dubbo，netty接收数据使用rpc传递给业务子系统处理

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
